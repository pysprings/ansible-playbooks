#!/usr/bin/bash

# activate pelican env
source /srv/website/env_pelican/bin/activate

# switch to website repo
pushd /srv/website/

# pull latest from git
git pull origin master

# switch to pelican dir
cd pelican

# build html
make html

# rsync html files
rsync -av /srv/website/pelican/output/ /var/www/html/

popd

deactivate

