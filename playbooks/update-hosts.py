- name: update hosts
  hosts: {{ target }}
  sudo: yes
  tags:
    - update

  vars_files:
  - /srv/ansible/pysprings/common/vars/global.yml
  - /srv/ansible/private/pysprings/{{ envtype }}.yml

  tasks:
  - include: {{ tasks }}/update_host.yml

  handlers:
  - include: {{ handlers }}/restart_services.yml

